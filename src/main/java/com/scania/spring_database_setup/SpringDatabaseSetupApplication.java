package com.scania.spring_database_setup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDatabaseSetupApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDatabaseSetupApplication.class, args);
    }

}
