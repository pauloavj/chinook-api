package com.scania.spring_database_setup.dataaccess;

import com.scania.spring_database_setup.models.domain.Customer;
import com.scania.spring_database_setup.models.dto.CustomerByCountryDto;
import com.scania.spring_database_setup.models.dto.MostPopularGenreByCustomerDto;
import com.scania.spring_database_setup.models.mapper.CustomersByCountryDtoMapper;
import com.scania.spring_database_setup.models.mapper.MostPopularGenreByCustomerDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.*;

@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;

    CustomersByCountryDtoMapper customersByCountryDtoMapper;
    MostPopularGenreByCustomerDtoMapper mostPopularGenreByCustomerDtoMapper;

    @Autowired
    public CustomerRepository(CustomersByCountryDtoMapper customersByCountryDtoMapper, MostPopularGenreByCustomerDtoMapper mostPopularGenreByCustomerDtoMapper) {
        this.customersByCountryDtoMapper = customersByCountryDtoMapper;
        this.mostPopularGenreByCustomerDtoMapper = mostPopularGenreByCustomerDtoMapper;
    }

    public List<Customer> getAllCustomers() {
        String sql = "SELECT" +
                " customer.customer_id," +
                " customer.first_name," +
                " customer.last_name," +
                " customer.country," +
                " customer.postal_code," +
                " customer.phone," +
                " customer.email" +
                " FROM" +
                " customer";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer getCustomer(int id) {
        Customer customer = null;
        String sql = "SELECT customer.customer_id, customer.first_name, customer.last_name, customer.country, customer.postal_code, customer.phone, customer.email FROM customer WHERE customer.customer_id = ?";
        try (Connection con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customer = new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public List<Customer> getCustomerByName(String firstName) {
        String sql = "SELECT customer.customer_id, customer.first_name, customer.last_name, customer.country, customer.postal_code, customer.phone, customer.email FROM customer WHERE customer.first_name LIKE ?";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, firstName);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add( new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<Customer> getCustomerByLimitAndOffset(int limit, int offset) {
        String sql = "SELECT customer.customer_id, customer.first_name, customer.last_name, customer.country, customer.postal_code, customer.phone, customer.email FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, limit);
            ps.setInt(2, offset);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add( new Customer(
                            rs.getInt("customer_id"),
                            rs.getString("first_name"),
                            rs.getString("last_name"),
                            rs.getString("country"),
                            rs.getString("postal_code"),
                            rs.getString("phone"),
                            rs.getString("email")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    public Customer addCustomer(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?)";
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getCountry());
            ps.setString(4, customer.getPostalCode());
            ps.setString(5, customer.getPhone());
            ps.setString(6, customer.getEmail());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public Customer updateCustomer(Customer customer, int id) {
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer.customer_id = ?";
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setString(3, customer.getCountry());
            ps.setString(4, customer.getPostalCode());
            ps.setString(5, customer.getPhone());
            ps.setString(6, customer.getEmail());
            ps.setInt(7, id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getCustomer(id);
    }

    public CustomerByCountryDto customersByCountry() {
        return customersByCountryDtoMapper.customerByCountry();
    }

    public Map<String,Float> highestSpenders() {
        String sql = "SELECT customer.first_name, SUM(inv.total) AS total_invoice FROM customer INNER JOIN invoice AS inv ON inv.customer_id = customer.customer_id GROUP BY customer.first_name ORDER BY total_invoice DESC";
        LinkedHashMap<String,Float> highestSpendersCustomers = new LinkedHashMap<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try(ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    highestSpendersCustomers.put(rs.getString("first_name"),rs.getFloat("total_invoice"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return highestSpendersCustomers;
    }

    public MostPopularGenreByCustomerDto mostPopularGenreByCustomer(int id) {
        return mostPopularGenreByCustomerDtoMapper.mostPopularGenreByCustomer(id);
    }
}