package com.scania.spring_database_setup.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;

@Getter
@Setter
@AllArgsConstructor
public class CustomerByCountryDto {
    private LinkedHashMap<String,Integer> customers;
}
