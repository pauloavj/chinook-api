package com.scania.spring_database_setup.models.mapper;

import com.scania.spring_database_setup.models.dto.MostPopularGenreByCustomerDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.LinkedHashMap;

@Component
public class MostPopularGenreByCustomerDtoMapper {
    @Value("${spring.datasource.url}")
    private String url;

    public MostPopularGenreByCustomerDto mostPopularGenreByCustomer(int id) {

        String sql = "SELECT g.\"name\" as genre_name, COUNT (il.invoice_line_id) as genre_total\n" +
                "FROM genre as g\n" +
                "INNER JOIN track as t ON t.genre_id = g.genre_id\n" +
                "INNER JOIN invoice_line as il ON il.track_id = t.track_id\n" +
                "INNER JOIN invoice as inv ON inv.invoice_id = il.invoice_id\n" +
                "INNER JOIN customer as c ON c.customer_id = inv.customer_id AND c.customer_id = ?\n" +
                "GROUP BY g.genre_id, g.\"name\"\n" +
                "ORDER BY genre_total DESC\n" +
                "FETCH FIRST 1 ROWS with TIES";
        LinkedHashMap<String, Integer> customers = new LinkedHashMap<>();
        try (Connection con = DriverManager.getConnection(url);
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    customers.put(rs.getString("genre_name"), rs.getInt("genre_total"));
                }
            }
        } catch (
                SQLException e) {
            e.printStackTrace();
        }
        return new MostPopularGenreByCustomerDto(customers);
    }
}

