package com.scania.spring_database_setup.models.mapper;

import com.scania.spring_database_setup.models.dto.CustomerByCountryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.LinkedHashMap;

@Component
public class CustomersByCountryDtoMapper {
    @Value("${spring.datasource.url}")
    private String url;

    public CustomerByCountryDto customerByCountry(){

        String sql = "SELECT customer.country, COUNT(customer.customer_id) AS number_of_customers FROM customer GROUP BY customer.country ORDER BY number_of_customers DESC";
        LinkedHashMap<String,Integer> customers = new LinkedHashMap<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try(ResultSet rs = ps.executeQuery()){
                while (rs.next()){
                    customers.put(rs.getString("country"),rs.getInt("number_of_customers"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new CustomerByCountryDto(customers);
    }
}
