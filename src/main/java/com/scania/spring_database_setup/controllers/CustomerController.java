package com.scania.spring_database_setup.controllers;

import com.scania.spring_database_setup.dataaccess.CustomerRepository;
import com.scania.spring_database_setup.models.domain.Customer;
import com.scania.spring_database_setup.models.dto.CustomerByCountryDto;
import com.scania.spring_database_setup.models.dto.MostPopularGenreByCustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        return new ResponseEntity<>(customerRepository.getAllCustomers(), HttpStatus.OK);
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Customer> getCustomers(@PathVariable int id) {
        return new ResponseEntity<>(customerRepository.getCustomer(id), HttpStatus.OK);
    }

    @GetMapping("/customers/byname/{firstName}")
    public ResponseEntity<List<Customer>> getCustomersByName(@PathVariable String firstName) {
        return new ResponseEntity<>(customerRepository.getCustomerByName(firstName), HttpStatus.OK);
    }

    @GetMapping("/customers/bylimitandoffset/{limit}/{offset}")
    public ResponseEntity<List<Customer>> getCustomerByLimitAndOffset(@PathVariable int limit, @PathVariable int offset) {
        return new ResponseEntity<>(customerRepository.getCustomerByLimitAndOffset(limit, offset), HttpStatus.OK);
    }
    @PostMapping("/customers")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer){
        return new ResponseEntity<>(customerRepository.addCustomer(customer), HttpStatus.CREATED);
    }

    @PatchMapping("/customers/{id}")
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer, @PathVariable int id){
        return new ResponseEntity<>(customerRepository.updateCustomer(customer, id), HttpStatus.CREATED);
    }

    @GetMapping("customers/byCountry")
    public ResponseEntity<CustomerByCountryDto> customersByCountry(){
        return new ResponseEntity<>(customerRepository.customersByCountry(), HttpStatus.OK);
    }

    @GetMapping("customers/highestSpenders")
    public ResponseEntity<Map<String,Float>> highestSpenders(){
        return new ResponseEntity<>(customerRepository.highestSpenders(), HttpStatus.OK);
    }

    @GetMapping("customers/mostPopularGenreByCustomer/{id}")
    public ResponseEntity<MostPopularGenreByCustomerDto> mostPopularGenreByCustomer(@PathVariable int id){
        return new ResponseEntity<>(customerRepository.mostPopularGenreByCustomer(id), HttpStatus.OK);
    }
}
